# Use an official Node.js runtime as a parent image
# FROM node AS build

# # Set the working directory inside the container
# WORKDIR /app

# # Copy package.json and package-lock.json to the container
# COPY package*.json ./

# # Install project dependencies
# RUN npm install

# # Copy the entire project directory to the container
# COPY . .

# Build your Vite project (change "build" to the appropriate script if needed)
# RUN npm run build

FROM nginx:1.21

COPY  ./dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx","-g", "daemon off;"]
