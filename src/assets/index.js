import logo_blue from "./logo_blue.png";
import logo_green from "./logo_green.png";
import menu from "./navbar/menu.svg";
import close from "./navbar/close.svg";
//about
import Backend from "./about/Backend.png";
import Bussines from "./about/Bussines.png";
import Datascience from "./about/Datascience.png";
import Frontend from "./about/Frontend.png";
//company
import cunninghamJournal from "./company/CunninghamJournal.png";
import sherlockLocksmith from "./company/SherlockLocksmith.png";
import wallmart from "./company/Wallmart.png";
//tech
import css from "./tech/css.png";
import docker from "./tech/docker.png";
import figma from "./tech/figma.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import redux from "./tech/redux.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";
import threejs from "./tech/threejs.svg";
import pyton from "./tech/python.png";
import java from "./tech/java.png";
import C from "./tech/c.png";
import MySQL from "./tech/MySQL.png";
import FastApi from "./tech/fastapi.png";
import nextJs from "./tech/NextJs.png";
import django from "./tech/django.jpeg";
import rabbitMQ from "./tech/rabbitMQ.png";
import webSocket from "./tech/webSocket.png";

//project
import carrent from "./project/carrent.png";
import jobit from "./project/jobit.png";
import tripguide from "./project/tripguide.png";
import scrumptious from "./project/scrumptious.png";
import carcar from "./project/carcar.png";
import alpha from "./project/alpha.png";
import netflix from "./project/netflix.png";
import matrix from "./project/matrix.png";
import movieRaiders from "./project/movieRaiders.png";
import ai from "./project/ai.png";
import TalkNow from "./project/TalkNow.png";
//github logo
import github from "./github.png";
//footer logo
import gitlab from "./footer/gitlab.png";
import linkedin from "./footer/linkedin.png";
//education
import utd from "./education/utd.png";
import tcc from "./education/tcc.png";
import ccc from "./education/ccc.png";
import mahatma from "./education/MahatmaGadingSchool.png";
import hackReactor from "./education/HackReactor.png";

export {
  logo_green,
  ai,
  TalkNow,
  movieRaiders,
  matrix,
  netflix,
  alpha,
  carcar,
  scrumptious,
  hackReactor,
  cunninghamJournal,
  sherlockLocksmith,
  wallmart,
  mahatma,
  ccc,
  tcc,
  utd,
  linkedin,
  gitlab,
  github,
  menu,
  close,
  logo_blue,
  Frontend,
  Backend,
  Bussines,
  Datascience,
  css,
  docker,
  figma,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  redux,
  tailwind,
  typescript,
  threejs,
  java,
  pyton,
  tripguide,
  jobit,
  carrent,
  C,
  MySQL,
  FastApi,
  nextJs,
  rabbitMQ,
  django,
  webSocket,
};
