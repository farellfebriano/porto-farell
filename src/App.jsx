import {
  MatrixEffect,
  NavBar,
  Hero,
  About,
  Experience,
  Tech,
  Projects,
  Contact,
  Footer,
} from "./components";
import { BrowserRouter } from "react-router-dom";
import { DeviceTypeProvider } from "./DeviceTypeContext";
import { ComputersCanvas } from "./components/canvas";
import { useEffect, useState } from "react";

const App = () => {
  return (
    <BrowserRouter>
      <MatrixEffect />
      <DeviceTypeProvider>
        <NavBar />
        <section>
          <ComputersCanvas />
          <Hero />
          <About />
          <Experience />
          <Tech />
          <Projects />
          <Contact />
          <Footer />
        </section>
      </DeviceTypeProvider>
    </BrowserRouter>
  );
};

export default App;
