const styles = {
  paddingX: "sm:px-16 px-6",
  paddingY: "sm:py-16 py-6",
  padding: "sm:px-16 px-6 sm:py-16 py-10",

  heroHeadText:
    "text-[#00ff0081] font-gugi lg:text-[80px] sm:text-[60px] xs:text-[60px] text-[40px] lg:leading-[98px] mt-2",
  heroSubText:
    "text-[#8af08a] font-medium lg:text-[30px] sm:text-[26px] xs:text-[20px] text-[16px] lg:leading-[40px]",

  sectionHeadText:
    " font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px] text-[#0f0] font-gugi ",
  sectionSubText: "sm:text-[18px] text-[14px] text-[#8af08a]  tracking-wider",
  sectionText:
    "mt-2 text-[#eae1e1] sm:text-[18px] text-[14px]  max-w-3xl sm:leading-[30px] leading-[25px] glass-green  p-7 rounded-[20px]",
};

export { styles };
