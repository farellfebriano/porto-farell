import React from "react";
import { motion } from "framer-motion";
import { staggerContainer } from "../utils/motion";
import { styles } from "../styles";

const sectionWrapper = (
  Component,
  idName // there is no {} because there is no returninng its a function inside a fucntion
) =>
  function HOC() {
    // so basically its a function that return another function

    return (
      <motion.section
        variants={staggerContainer()}
        initial="hidden"
        whileFocus="show"
        whileInView="show"
        viewport={{ once: true, amount: 0.25 }}
        className={`${styles.padding} max-w-7xl mx-auto relative z-0`}
      >
        <span className="hash-span" id={idName}>
          &nbsp;
        </span>
        <Component />
      </motion.section>
    );
  };

export default sectionWrapper;

/*
HOC -->
In React, a Higher-Order Component (HOC) is a design
pattern used to enhance or extend the functionality
of a component. It's not a component itself but
rather a function that takes a component and
returns a new component with additional props or
behavior. HOCs are a way to reuse component logic
and can be thought of as functions that "wrap"
around other components to provide them with
additional functionality.
*/
