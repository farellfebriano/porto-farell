import React from "react";
import { motion } from "framer-motion";
import { staggerContainer } from "../utils/motion";

const navBarWraper = (
  Component,
  idName // there is no {} because there is no returninng its a function inside a fucntion
) =>
  function HOC() {
    // so basically its a function that return another function

    return (
      <motion.section
        variants={staggerContainer()}
        initial="hidden"
        whileInView="show"
        viewport={{ once: true, amount: 0.25 }}
      >
        <Component />
      </motion.section>
    );
  };

export default navBarWraper;
