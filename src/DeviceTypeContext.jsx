// DeviceTypeContext.js
import React, { createContext, useContext, useEffect, useState } from "react";

const DeviceTypeContext = createContext(null);

export function DeviceTypeProvider({ children }) {
  const [isMobile, setIsMobile] = useState(
    /Mobi/i.test(window.navigator.userAgent)
  );

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(/Mobi/i.test(window.navigator.userAgent));
    };
    window.addEventListener("resize", handleResize);

    return () => {
      // Cleanup the event listener when the component unmounts

      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <DeviceTypeContext.Provider value={{ isMobile }}>
      {children}
    </DeviceTypeContext.Provider>
  );
}

export function useDeviceType() {
  return useContext(DeviceTypeContext);
}
