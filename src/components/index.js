import About from "./About";
import Hero from "./Hero";
import MatrixEffect from "./MatrixEffect";
import NavBar from "./NavBar";
import Loader from "./Loader";
import { useDeviceType } from "../DeviceTypeContext";
import Experience from "./Experience";
import Tech from "./Tech";
import Projects from "./Projects";
import Contact from "./Contact";
import Footer from "./Footer";

export {
  Contact,
  Experience,
  About,
  Hero,
  NavBar,
  MatrixEffect,
  Loader,
  useDeviceType,
  Tech,
  Projects,
  Footer,
};
