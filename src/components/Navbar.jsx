/* eslint-disable react-refresh/only-export-components */
import { useState } from "react";
import { Link } from "react-router-dom";
import { logo_blue, menu, close } from "../assets";
import { styles } from "../styles";
import { navLinks } from "../constant";
import { motion } from "framer-motion";
import { fadeIn } from "../utils/motion";
import { navBarWraper } from "../hoc";
const Navbar = () => {
  const [active, setActive] = useState("");
  const [toggle, setToggle] = useState(false);
  return (
    <motion.nav
      variants={fadeIn("right", "spring", 0, 0.5)}
      className={`${styles.paddingX} w-full flex items-center
    py-5  top-0 z-30 bg-ffffff1a`}
    >
      <div className=" w-full flex justify-between items-center max-w-7xl mx-auto">
        {/* the name */}
        <Link
          to="/"
          className="flex items-center gap-2 "
          onClick={() => {
            setActive("");
            window.scrollTo(0, 0);
          }}
        >
          <div>
            <img
              src={logo_blue}
              alt="logo"
              className="w-14 h-14 object-contain"
            />
          </div>
          <span className="sm:block hidden text-whte">| Farell Febriano</span>
        </Link>
        {/* the name */}
        {/* the navigation */}
        <ul className="list-none hidden sm:flex flex-row gap-10">
          {navLinks.map((link) => {
            return (
              <li
                className={`${
                  active === link.title ? "text-white" : " text-green-500"
                } hover:text-slate-500 [18px] font-medium cursor-pointer`}
                key={link.id}
                onClick={() => {
                  setActive(link.id);
                }}
              >
                <a
                  className={`${
                    active === link.id ? "text-white" : " text-green-500"
                  }`}
                  href={`#${link.id}`}
                >
                  {link.title}{" "}
                </a>
              </li>
            );
          })}
        </ul>
        {/* the navigation */}
        {/* the minimized navigation bar */}
        <div className="sm:hidden flex flex-1 justify-end items-center  ">
          <img
            src={toggle ? close : menu}
            alt="menu"
            className="w-[28px] h-[28px] object-contain cursor-pointer transform "
            onClick={() => setToggle(!toggle)}
          ></img>
        </div>
        {/* the minimized navigation bar */}
        {/* expanded list of navigation bar */}
        {toggle && (
          <div
            className={`${
              !toggle ? "hidden" : "flex"
            } p-6 black-gradient absolute top-16 right-0 mx-4 my-2 min-w-[140px] z-10 rounded-xl sm:hidden z-30`}
          >
            <ul className="list-none flex gap-10 justify-end flex-col gap-4">
              {navLinks.map((link) => {
                return (
                  <li
                    className={`${
                      active === link.title ? "text-green-500" : "text-white "
                    }[18px] font-medium cursor-pointer`}
                    key={link.id}
                  >
                    <a
                      href={`#${link.id}`}
                      onClick={() => {
                        setToggle(!toggle);
                        setActive(link.id);
                      }}
                      className={`${
                        active === link.id ? " text-green-500" : "text-white "
                      }  font-poppins font-medium cursor-pointer text-[16px]`}
                    >
                      {link.title}
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    </motion.nav>
  );
};

export default navBarWraper(Navbar);
