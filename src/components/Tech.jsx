import React from "react";
import { BallCanvas } from "./canvas";
import { technologies } from "../constant";
import { styles } from "../styles";
import { sectionWrapper } from "../hoc";
import { textVariant, fadeIn } from "../utils/motion";
import { motion } from "framer-motion";

const Tech = () => {
  return (
    <>
      <motion.div variants={textVariant(0.2)}>
        <p className={`${styles.sectionSubText}`}>experience</p>
        <h2 className={`${styles.sectionHeadText}`}>Tech.</h2>
      </motion.div>
      <motion.p
        variants={fadeIn("", "", 0.1, 1)}
        className={`${styles.sectionText}`}
      >
        What I like the most about the software engineering industry is that a
        big portion of the knowledge is open source. This leads me to believe
        that to become a proficient software engineer, one needs only these four
        essential qualities: a sincere curiosity about what you are learning, a
        clear understanding of what you want to learn, the commitment to start
        your learning journey, and the discipline to keep learning and grow.
        These four qualities have been instrumental in my continuous growth as a
        software engineer. Below, I outline the tech I've learned throughout my
        journey in the software engineering field so far.
      </motion.p>
      <div className="flex  justify-center sm:gap-7 content-center flex-wrap  mt-[40px]  h-full p-6 rounded-[20px] glass">
        {technologies.map((technology, index) => {
          return (
            <motion.div
              className="sm:w-28 w-24 h-full"
              key={index}
              variants={fadeIn("right", "spring", index * 0.2)}
            >
              <BallCanvas icon={technology.icon} />
              <h3 className="sm:text-[16px]  text-[14px] text-center font-poppins text-secondary">
                {technology.name}
              </h3>
            </motion.div>
          );
        })}
      </div>
    </>
  );
};

export default sectionWrapper(Tech, "tech");
