import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";
import { services } from "../../constant";
const About_Corousel = ({ props }) => {
  const [isLarge, setLarge] = useState(false);
  useEffect(() => {
    // add listener for change to the screen size
    const mediaQuery = window.matchMedia("(max-width:640px)");
    // set initial value of the 'ismobile'
    setLarge(mediaQuery.matches);
    //define a callback fuction to handle changes to the media query
    const handleMediaQueryChange = (event) => {
      setLarge(event.matches);
    };
    //add a callback function as a listener for changes to the media query
    mediaQuery.addEventListener("change", handleMediaQueryChange);
    return () => {
      // remove the listener when the component is unmounted
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  return (
    <div className="flex item-center justify-center flex-col">
      <Swiper
        slidesPerView={isLarge ? 1 : 3}
        centeredSlides={true}
        spaceBetween={0}
        grabCursor={true}
        pagination={{
          clickable: true,
        }}
        modules={[Pagination]}
        className="mySwiper w-full "
      >
        {services.map((service, index) => {
          return (
            <SwiperSlide key={index}>
              <div className="xs:w-[250px] w-[217px] mx-auto mt-[80px] ">
                <div className="w-full glass p-[1px] rounded-[20px] ">
                  <div className="rounded-[20px] py-5 px-12 min-h-[280px] flex-col flex items-center justify-evenly">
                    <img
                      src={service.icon}
                      className="w-16 h-16 object-contain"
                    ></img>
                    <h3 className="text-center text-white font-b">
                      {service.title}
                    </h3>
                  </div>
                </div>
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};

export default About_Corousel;
