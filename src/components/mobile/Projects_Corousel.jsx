import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";
import { projects } from "../../constant";
import { github } from "../../assets";

const Projects_Corousel = ({ props }) => {
  return (
    <Swiper
      slidesPerView={1}
      centeredSlides={true}
      spaceBetween={50}
      grabCursor={true}
      pagination={{
        clickable: false,
      }}
      modules={[Pagination]}
      className="mySwiper w-full  mt-10"
    >
      {projects[props].map((project) => {
        return (
          <SwiperSlide key={project.name}>
            <div className="glass p-5 rounded-2xl w-[330px] h-[496px] m-auto flex-row ">
              <div className="relative w-full h-[200px]">
                <img
                  src={project.image}
                  alt={project.name}
                  className="w-full h-full object-cover rounded-2xl"
                />
                <div className=" absolute inset-0 flex justify-end m-3 card-img_hover">
                  {/* inset is the game change over here */}
                  <div
                    onClick={() =>
                      window.open(project.source_code_link, "blanlk")
                    }
                    className=" w-10 h-10 rounded-full flex justify-center items-center cursor-pointer bg-black"
                  >
                    <img
                      src={project.rep}
                      className="objec-contain h-1/2 w-1/2"
                    />
                  </div>
                </div>
              </div>
              <div className="mt-5">
                <h3 className=" font-semibold text-[#0f0] text-[16px]">
                  {project.name}
                </h3>
                <p className="mt-2 text-secondary text-[13px] leading-[25px]">
                  {project.description}
                </p>
              </div>
              <div className="mt-4 flex flex-wrap gap-2  bottom-4 absolute ">
                {project.tags?.map((tag, index) => {
                  return (
                    <p key={index} className={`text-[12px] ${tag.color}`}>
                      #{tag.name}
                    </p>
                  );
                })}
              </div>
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default Projects_Corousel;
