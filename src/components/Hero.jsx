import React, { useEffect, useState } from "react";
import { styles } from "../styles";
import { useTypewriter, Cursor } from "react-simple-typewriter";
import { navBarWraper } from "../hoc";
import { useDeviceType } from "../DeviceTypeContext";

const Hero = () => {
  let device = useDeviceType();
  // const [isMobile, setIsMobile] = useState(false);
  const word = ["Matrix", "Portfolio"];
  const [text] = useTypewriter({
    words: word,
    typeSpeed: 50,
    deleteSpeede: 60,
  });
  useEffect(() => {
    // setIsMobile(/Mobi/i.test(window.navigator.userAgent));
  }, [device.isMobile]);

  return (
    /* need to change the location when its in */
    <div className="relative w-full mx-auto sm:h-[1300px] h-[550px] ">
      <div
        className={`relative inset-0  max-w-7xl mx-auto ${styles.paddingX} flex  justify-center p-auto gap-5 pb-[px]`}
      >
        <div className=" mt-[20%]">
          <h1 className="text-center font-glass font- ">
            FULL-STACK DEVELOPER
          </h1>
          <h1 className={`${styles.heroHeadText} text-white text-center`}>
            Hello, welcome to {device.isMobile ? "" : "my"}
          </h1>
          <h1
            className={`${styles.heroHeadText}text-[#0f0] font-gugi text-center`}
          >
            {device.isMobile ? (
              <span className=" text-white font-gugi">my </span>
            ) : (
              ""
            )}
            {text}
            <span className="text-[#00ff0081] font-gugi">
              <Cursor cursorStyle="|" />
            </span>
          </h1>
          <div
            className={`flex justify-center gap-10 font-glass sm:text-[16px] text-[12px]`}
          >
            <span>Farell</span>
            <span>Febriano</span>
            <span>Production</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default navBarWraper(Hero);
