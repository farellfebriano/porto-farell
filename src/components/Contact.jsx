import React, { useState, useRef, useEffect } from "react";
import { motion } from "framer-motion";
import emailjs from "@emailjs/browser";
import { styles } from "../styles";
import { sectionWrapper } from "../hoc";
import { slideIn } from "../utils/motion";
//templateid-->template_kj3a6kb
//serviceId-->service_i1id6rt
//publickey--> JtjWnQJD8439vn1IB

const Contact = () => {
  const serviceId = import.meta.env.VITE_EMAIL_SERVICE_ID;
  const templateId = import.meta.env.VITE_EMAIL_TEMPLATE_ID;
  const publicId = import.meta.env.VITE_EMAIL_PUBLIC_ID;
  const email = import.meta.env.VITE_CONTACT_EMAIL;
  const formRef = useRef();
  const [form, setForm] = useState({
    name: "",
    email: "",
    message: "",
  });
  const [loading, setLoading] = useState(false);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    emailjs
      .send(
        serviceId,
        templateId,
        {
          from_name: form.name,
          to_name: "farell",
          from_email: form.email,
          to_email: email,
          message: form.message,
        },
        publicId
      )
      .then(
        () => {
          setLoading(false);
          alert("Thank you. I will get back to you as soon as possible.");
          setForm({
            name: "",
            email: "",
            message: "",
          });
        },
        () => {
          setLoading(false);
          alert("something went wrong");
        }
      );
  };
  return (
    <div className="xl:mt-12 xl:flex-row flex-col-reverse flex gap-10 overflow-hidden">
      <motion.div
        variants={slideIn("left", "tween", 0.1, 0.5)}
        className="flex-[0.75] glass-green p-8 rounded-2xl mx-auto w-full "
      >
        <p className={`${styles.sectionSubText}`}> get in touch</p>
        <h3 className={`${styles.sectionHeadText}`}>Contact.</h3>
        <form
          ref={formRef}
          className="sm:mt-10 mt-5 flex flex-col gap-5"
          onSubmit={handleSubmit}
        >
          <label className="flex flex-col">
            <span className="text-white font-medium mb-2">Name</span>
            <input
              type="text"
              name="name"
              value={form.name}
              onChange={handleChange}
              placeholder="what's your name"
              className="sm:py-4 py-1.5 px-6 placeholder:text-secondary text-white rounded-[20px] outline-none font-me border-[#f0f]"
            />
          </label>
          <label className="flex flex-col">
            <span className="text-white font-medium mb-2">Email</span>
            <input
              type="email"
              name="email"
              value={form.email}
              onChange={handleChange}
              placeholder="what's your email"
              className="sm:py-4 py-1.5 px-6 placeholder:text-secondary text-white rounded-[20px] outline-none font-me border-[#f0f]"
            />
          </label>
          <label className="flex flex-col">
            <span className="text-white font-medium mb-2">Message</span>
            <textarea
              rows="5"
              name="message"
              value={form.message}
              onChange={handleChange}
              placeholder="what's your message "
              className="sm:py-4 py-1.5 px-6 placeholder:text-secondary text-white rounded-[20px] outline-none font-me border-[#f0f]"
            />
          </label>
          <button
            type="submit"
            className="glass-green sm:py-2 py-1 sm:px-8 px-4 sm:text-[16px] text-[14px] outline-none w-fit text-white font-bold shadow-md shadow-primary rounded-[10px] ml-[2%]"
          >
            {loading ? "Sending ..." : "Send"}
          </button>
        </form>
      </motion.div>
    </div>
  );
};

export default sectionWrapper(Contact, "contact");
