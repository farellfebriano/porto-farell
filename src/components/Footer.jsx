import React from "react";
import { Link } from "react-router-dom";
import { footerLinks, footerAccount } from "../constant";
import { useDeviceType } from "../DeviceTypeContext";
const Footer = () => {
  let device = useDeviceType();
  return (
    <footer className="text-white bg-transparent bg-opacity-20 backdrop-filter backdrop-blur-md">
      <hr className="my-8 border-gray-800" />
      <div className="container mx-auto py-8 px-6">
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
          <div>
            <h2 className="sm:text-2xl text-l font-bold sm:mb-4 text-[#0f0]">
              Farell Portofolio {device.height}
            </h2>
            <p className="text-gray-400 sm:text-[16px] text-sm">
              Thank you for taking the time to review this<br></br> portfolio. I
              eagerly anticipate the opportunity <br></br> to collaborate with
              you in the future.
            </p>
            <div className="flex mt-4">
              {/* this is where the logo will be */}
              {footerAccount.map((account, index) => {
                return (
                  <a
                    key={index}
                    href={account.link}
                    className="mr-4 text-gray-400 hover:text-green-500"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img
                      src={account.logo}
                      className="h-[32px] w-[32px] object-contain"
                    ></img>
                  </a>
                );
              })}
            </div>
          </div>
          {/* links harus di tengah */}
          <div className="grid sm:justify-center justify-start">
            <h3 className="sm:text-2xl text-l font-bold sm:mb-4 text-[#0f0]">
              Links
            </h3>
            <ul className="text-gray-400 p-aut">
              {footerLinks.map((link, index) => {
                return (
                  <li key={index} className="">
                    <a
                      to="home"
                      href={`#${link.id}`}
                      className="hover:text-green-500 cursor-pointer sm:text-[16px] text-sm"
                    >
                      {link.title}
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
          <div className=" grid sm:justify-end justify-start">
            <div>
              <h3 className=" sm:text-2xl text-l font-bold sm:mb-4 text-[#0f0] ">
                Contact Me
              </h3>
              <p className="text-gray-400 mb-2 sm:text-[16px] text-sm">
                farellfebriano8@gmail.com
              </p>
            </div>
          </div>
        </div>
        <hr className="my-8 border-[#0f0]" />
        <p className="text-center text-gray-400 sm:text-[16px] text-sm">
          &copy; Farell Production. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
