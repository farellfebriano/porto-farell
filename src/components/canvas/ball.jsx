import React, { Suspense, useEffect, useState } from "react";
import { Canvas } from "@react-three/fiber";
import {
  Float,
  OrbitControls,
  Preload,
  useGLTF,
  useTexture,
  Decal,
} from "@react-three/drei";
import CanvasLoader from "../Loader";
import { useDeviceType } from "../../DeviceTypeContext";

const Ball = (props) => {
  const device = useDeviceType();
  const [decal] = useTexture([props.imgUrl]); // using distructure tecnique in javascript
  return (
    <Float speed={1.75} rotationIntensity={2} floatIntensity={1}>
      <ambientLight intensity={0.25} />
      <directionalLight position={[0, 0, 0.05]} />
      <mesh castShadow receiveShadow scale={device.isMobile ? 1.7 : 2.45}>
        <dodecahedronGeometry args={[1, 1]} />
        <meshStandardMaterial
          color={"#ffffff"}
          polygonOffset
          polygonOffsetFactor={-5}
          flatShading
        />
        <Decal
          position={[0, 0, 1]}
          rotation={[2 * Math.PI, 0, 6.25]}
          scale={1}
          flatShading
          map={decal}
        />
      </mesh>
    </Float>
  );
};

const BallCanvas = ({ icon }) => {
  return (
    <Canvas frameloop="always" gl={{ preserveDrawingBuffer: true }}>
      <Suspense fallback={<CanvasLoader />}>
        <OrbitControls enableZoom={false} />
        <Ball imgUrl={icon} />
      </Suspense>
      <Preload all />
    </Canvas>
  );
};

export default BallCanvas;

/*
NOTE -->
 <Preload all /> means
 In summary, this component helps optimize the
 loading and performance of 3D graphics by precompiling
 materials, and you can control whether it preloads all
 materials or just those currently visible. This is useful
 for ensuring a responsive and smooth user experience in your
 3D application.


*/
