import React, { Suspense, useEffect, useState } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Preload, useGLTF, Float } from "@react-three/drei";
import CanvasLoader from "../Loader";
import { useDeviceType } from "../../DeviceTypeContext";
const Computers = ({ isMobile }) => {
  const computer = useGLTF("./sentinelle/scene.gltf");
  return (
    <Float speed={9} rotationIntensity={0.1} floatIntensity={0.5}>
      <mesh>
        <hemisphereLight intensity={8} groundColor="black" />
        <pointLight position={[0, 4, 4]} intensity={19} distance={0} />
        <spotLight
          position={[-0, 0, 0]}
          angle={0}
          penumbra={1}
          intensity={6}
          castShadow
          shadow-mapsize={1024}
        />
        <primitive
          object={computer.scene}
          scale={isMobile ? 0.54 : 0.7}
          position={isMobile ? [0, 0.5, -0.5] : [0, -0.7, -1.5]}
          rotation={[0, 0, 0]}
        />
      </mesh>
    </Float>
  );
};

const ComputersCanvas = () => {
  let device = useDeviceType();
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    // add listener for change to the screen size
    const mediaQuery = window.matchMedia("(max-width:640px)");
    // set initial value of the 'ismobile'
    setIsMobile(mediaQuery.matches);
    //define a callback fuction to handle changes to the media query
    const handleMediaQueryChange = (event) => {
      setIsMobile(event.matches);
    };
    //add a callback function as a listener for changes to the media query
    mediaQuery.addEventListener("change", handleMediaQueryChange);
    return () => {
      // remove the listener when the component is unmounted
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  return (
    <Canvas
      frameloop="always"
      shadows
      dpr={[1, 2]}
      camera={{ position: [20, 3, 5], fov: 25 }}
      gl={{ preserveDrawingBuffer: true }}
      style={{
        height: isMobile ? 1000 : 1800,
        position: "absolute",
        zIndex: device.isMobile ? 0 : 20,
      }}
    >
      <Suspense fallback={<CanvasLoader />}>
        <OrbitControls
          enableZoom={false}
          maxPolarAngle={Math.PI / 2}
          minPolarAngle={Math.PI / 2}
        />
        <Computers isMobile={isMobile} />
      </Suspense>

      <Preload all />
    </Canvas>
  );
};

export default ComputersCanvas;

/*
1.) what is useGLTF ?-->
it is commonly found in libraries like Three.js
a popular JavaScript library for creating 3d graphics
and visualization in the web browser

2.) what is <mesh> tag ?-->
However, in the context of 3D graphics and 3D modeling,
the term "mesh" is commonly used to refer to the structure
that defines the shape of a 3D object. A mesh is made up of
vertices (points in 3D space), edges (lines connecting vertices),
and faces (polygons formed by connecting edges). In this context,
a mesh is not an HTML tag but rather a fundamental concept in 3D
graphics and modeling.

3.) what is hemisphereLight ?-->
A light source positioned directly above the scene,
with color fading from the sky color to the ground color.

4.) what is pointLight ?-->
A light that gets emitted from a single point in all directions.
A common use case for this is to replicate the light emitted from
a bare lightbulb.

5.) what is primitive tags
In Three.js, the <primitive> element is used to create a primitive
3D object by specifying an existing Three.js object, such as a geometry
or a mesh, as the source. It allows you to reuse and manipulate existing
3D objects within your Three.js scene.

NOTE --> it is actualy the light that give glare to the computer

5.) what is preserveDrawingBuffer: true ?

is an option that you can pass when creating a WebGL renderer. This option determines whether
the WebGL drawing buffer should be preserved or not after rendering each frame. The drawing
buffer is the internal memory where the rendered image is stored before it's displayed on the screen.

Here's what { preserveDrawingBuffer: true } means:

Preserving the Drawing Buffer: When you set preserveDrawingBuffer to true,
it means that the WebGL renderer will retain the contents of the drawing
buffer after rendering a frame. This can be useful in certain situations
where you want to access or capture the rendered image after it's been drawn,
such as taking screenshots of the 3D scene. Default Behavior: By default,
the value of preserveDrawingBuffer is often false. This means that the drawing
buffer is cleared after rendering each frame, and you won't be able to access the
previously rendered frame. Setting it to true ensures that the buffer is preserved
for further operations.

6.) <Suspense fallback={<CanvasLoader /> ?
 is used to wrap components that may have asynchronous
 operations, such as data fetching, dynamic imports, or
 any other asynchronous task. These asynchronous tasks are
 often used to load data or code chunks lazily, which can improve
 the performance of your application.

 NOTE --> since the canvasloader is not yet created this will create error u need
 to change it into null to avoid it


*/
