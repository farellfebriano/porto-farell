import { useEffect, useRef } from "react";

const MatrixRainingCode = () => {
  const canvasRef = useRef(null);
  const isMounted = useRef(true);
  const count = useRef(0);

  useEffect(() => {
    // initialize the canvas
    const isMobile = /Mobi/i.test(window.navigator.userAgent);
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    let width = (canvas.width = window.innerWidth);
    let height = (canvas.height = window.innerHeight);
    const font = isMobile ? 16 : 18;
    let columns = Math.floor(width / font);
    const characters =
      "アァカサタナハマヤャラワガザダバパイィキシチニヒミリヰギジヂビピウゥクスツヌフムユュルグズブヅプエェケセテネヘメレヱゲゼデベペオォコソトノホモヨョロヲゴゾドボポヴッンㄱㄴㄷㄹㅁㅂㅅㅈㅊㅋㅌㅍㅎㅏㅑㅓㅕㅗㅜㅠㅡ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let drops = [];
    // initiate how many coloumns
    const initiate = () => {
      for (let i = 0; i < columns; i++) {
        drops[i] = Math.floor(Math.random() * (height * 1));
      }
    };
    initiate();

    // the function that is paint the canvas
    const draw = () => {
      ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
      ctx.fillRect(0, 0, width, height);
      ctx.fillStyle = "#0f0";
      ctx.font = font + "px monospace";
      ctx.textAllign = "center";
      for (let i = 0; i < drops.length; i++) {
        const text = characters.charAt(
          Math.floor(Math.random() * characters.length)
        );
        ctx.fillText(text, i * 20, drops[i] * 20);
        if (drops[i] * font > height && Math.random() > 0.9) {
          drops[i] = 0;
        }
        drops[i]++;
      }
    };

    let lastTime = 0;
    const fps = 20;
    const nextFrame = 1000 / fps;
    let timer = 0;

    const animation = (timeStamp) => {
      if (isMounted.current) {
        // delay depanding the fps
        let deltaTime = timeStamp - lastTime;

        lastTime = timeStamp;
        if (timer > nextFrame) {
          draw();
          timer = 0;
        } else {
          timer += deltaTime;
        }
        requestAnimationFrame(animation);
      } else {
        count.current = 0;
        isMounted.current = true;
        return;
      }
    };
    animation(0);

    const resizeHandler = () => {
      width = canvas.width = window.innerWidth;
      height = canvas.height = window.innerHeight;

      let columns = Math.floor(width / 20);
      drops = [];

      for (let i = 0; i < columns; i++) {
        drops[i] = Math.floor(Math.random() * height);
      }

      isMounted.current = false;
      requestAnimationFrame(animation);
    };

    window.addEventListener("resize", resizeHandler);

    return () => {
      window.removeEventListener("resize", resizeHandler);
      isMounted.current = false;
    };
  }, []);

  return (
    <canvas
      className="matrix-canvas fixed top-0 left-0 z-[-1] bg-black"
      ref={canvasRef}
    ></canvas>
  );
};

export default MatrixRainingCode;
