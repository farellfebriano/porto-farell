import React from "react";

import { sectionWrapper } from "../hoc";
import { projects } from "../constant";
import { textVariant, fadeIn } from "../utils/motion";
import { motion } from "framer-motion";
import { styles } from "../styles";
import { Tilt } from "react-tilt";
import { useState } from "react";
import { useDeviceType } from "../DeviceTypeContext";
import { Projects_Corousel } from "./mobile";
const ProjectCard = ({
  index,
  name,
  description,
  tags,
  image,
  source_code_link,
  link,
  rep,
}) => {
  return (
    <Tilt
      options={{
        max: 45,
        scale: 1,
        speed: 450,
      }}
      className="glass p-5 rounded-2xl w-[360px] h-[496px] "
    >
      <div className="relative w-full h-[230px]">
        <img
          src={image}
          alt={name}
          className="w-full h-full object-cover rounded-2xl"
        />
        <div className=" absolute inset-0 flex justify-end m-3 card-img_hover">
          {/* inset is the game change over here */}
          <div
            onClick={() => window.open(source_code_link, "blank")}
            className=" w-10 h-10 rounded-full flex justify-center items-center cursor-pointer bg-black "
          >
            <img src={rep} className="objec-contain h-1/2 w-1/2" />
          </div>
        </div>
      </div>
      <div className="flex-col">
        <h3 className=" font-semibold text-[#0f0] text-[24px]">{name}</h3>
        <p className="mt-2 text-secondary text-[13px] grow my-auto">
          {description}
        </p>
        <div className="mt-4 flex  flex-wrap gap-2 items-end">
          {tags.map((tag, index) => {
            return (
              <p key={index} className={`text-[14px] ${tag.color}`}>
                #{tag.name}
              </p>
            );
          })}
        </div>
      </div>
    </Tilt>
  );
};

const Projects = () => {
  const device = useDeviceType();
  const [object, setObject] = useState("Recent");
  return (
    <>
      <motion.div variants={textVariant()}>
        <p className={styles.sectionSubText}>project experience</p>
        <h2 className={styles.sectionHeadText}>{object} Projects.</h2>
      </motion.div>
      <div className="w-full flex">
        <motion.p
          variants={fadeIn("", "", 0.1, 1)}
          className={`${styles.sectionText}  `}
        >
          The key to improving coding is through the act of coding itself. After
          completing the coding boot camp, I remained active in developing new
          projects and exploring new languages and frameworks. Below, you'll
          find a compilation of my individual and collaborative projects, sorted
          from past,recent to future projects.
        </motion.p>
      </div>
      <motion.div
        className="container-glass_button justify-center sm:mt-10 mt-5"
        variants={textVariant(0.3)}
      >
        <div className="btn ">
          <button
            value={"Past"}
            onClick={(e) => {
              setObject(e.target.value);
            }}
          >
            Past
          </button>
        </div>
        <div className="btn ">
          <button
            value={"Recent"}
            onClick={(e) => {
              setObject(e.target.value);
            }}
          >
            Recent
          </button>
        </div>
        <div className="btn">
          <button
            value={"Future"}
            onClick={(e) => {
              setObject(e.target.value);
            }}
          >
            Future
          </button>
        </div>
      </motion.div>
      <div className="flex w-full h-full flex justify-center">
        {device.isMobile ? (
          <Projects_Corousel props={object} />
        ) : (
          <div className="mt-20 flex flex-wrap gap-7">
            {projects[object].map((project, index) => (
              <ProjectCard key={index} {...project} index={index} />
            ))}
          </div>
        )}
      </div>
    </>
  );
};

export default sectionWrapper(Projects, "projects");

/* NOTE -->
1.)The inset CSS property is a shorthand that
corresponds to the top, right, bottom, and/or left
properties. It has the same multi-value syntax of the
margin shorthand.

2.) <div onClick={() => window.open(source_code, "blanlk")}></div>
will open another window when is clicked
*/
