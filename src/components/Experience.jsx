import React, { useEffect, useState } from "react";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import { color, motion } from "framer-motion";
import "react-vertical-timeline-component/style.min.css";
import { sectionWrapper } from "../hoc";
import { textVariant, fadeIn } from "../utils/motion";
import { styles } from "../styles";
import { experiences, experienceStory } from "../constant";

const ExpereinceCard = ({ experience }) => (
  // if we use () it returns imidietly or automatically
  <VerticalTimelineElement
    contentArrowStyle={{ borderRight: "7px solid  #0f0" }}
    contentStyle={{
      color: "#dacccc",
    }}
    date={experience.date}
    iconStyle={{
      background: "white",
      color: "#0f0",
    }}
    icon={
      <div className="flex justify-center items-center w-full h-full">
        <img
          src={experience.icon}
          alt={experience.company_name}
          className="object-contain w-[80%] h-[80%]"
        ></img>
      </div>
    }
  >
    <div className="">
      <h3 className="text-[#0f0] text-[18px] font-semibold ">
        {experience.title}
      </h3>
      <p
        className=" text-[#00ff0083] text-[15px] font-semibold "
        style={{ margin: 0 }}
      >
        {experience.company_name}
      </p>
    </div>
    <ul className="mt-5 list-disc ml-5 space-y-2">
      {experience.points.map((point, index) => {
        return (
          <li
            key={index}
            className=" text-[#eaeaea] text-[14px] pl-1 tracking-wider"
          >
            {point}
          </li>
        );
      })}
    </ul>
  </VerticalTimelineElement>
);

const Experience = () => {
  const [object, setObject] = useState("Education");
  useEffect(() => {}, [object]);

  return (
    <>
      <motion.div variants={textVariant(0)}>
        {" "}
        {/* //using the tax variant and the initial will be propagate from section wraper */}
        <motion.div variants={textVariant(0)}>
          <p className={`${styles.sectionSubText}`}>work & education</p>
          <h2 className={`${styles.sectionHeadText}`}>{object} Experience.</h2>
        </motion.div>
        <motion.p
          variants={fadeIn("", "", 0.1, 1)}
          className={`${styles.sectionText}`}
        >
          {experienceStory[object]}
        </motion.p>
        <div className="container-glass_button justify-center sm:mt-10 mt-5">
          <div className="btn ">
            <button
              value={"Education"}
              onClick={(e) => {
                setObject(e.target.value);
              }}
            >
              Education
            </button>
          </div>
          <div className="btn">
            <button
              value={"Work"}
              onClick={(e) => {
                setObject(e.target.value);
              }}
            >
              Work
            </button>
          </div>
        </div>
      </motion.div>
      <div className="sm:mt-18 mt-10  flex flex-col">
        <VerticalTimeline lineColor="rgba(0, 255, 0, 0.383)">
          {experiences[object].map((experience, index) => {
            return (
              <ExpereinceCard
                key={index}
                index={index}
                experience={experience}
              />
            );
          })}
        </VerticalTimeline>
      </div>
    </>
  );
};
export default sectionWrapper(Experience, "experience");
