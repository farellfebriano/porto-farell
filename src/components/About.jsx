import React, { useEffect, useState } from "react";
import { Tilt } from "react-tilt";
import { motion } from "framer-motion";
import { styles } from "../styles";
import { textVariant, fadeIn } from "../utils/motion";
import { services } from "../constant";
import { sectionWrapper } from "../hoc";
import About_Corousel from "./mobile/About_Corousel";
// slider dependency
import { useDeviceType } from "../DeviceTypeContext";

const ServiceCard = ({ index, title, icon }) => {
  return (
    <Tilt
      className="xs:w-[250px] w-[217px]"
      options={{ max: 45, scale: 1, speed: 450, reverse: true }}
    >
      <motion.div
        // i dont know where he put the 'card-shadow' at
        // Note so basically this is div inside the div the inside div is gradient and the fron kinda overlaping eachother
        variants={fadeIn("right", "spring", index * 0.2)}
        className="w-full glass p-[1px] rounded-[20px] "
      >
        <div className="rounded-[20px] py-5 px-12 min-h-[280px] flex-col flex items-center justify-evenly">
          <img src={icon} className="w-16 h-16 object-contain"></img>
          <h3 className="text-center text-white font-b">{title}</h3>
        </div>
      </motion.div>
    </Tilt>
  );
};

const About = () => {
  let device = useDeviceType();

  return (
    <div className=" relative ">
      {/* just to create little bit space with the 3d */}

      <motion.div variants={textVariant(0.2)}>
        <p className={`${styles.sectionSubText}`}>introduction</p>
        <h2 className={`${styles.sectionHeadText}`}>About Me.</h2>
      </motion.div>
      <motion.p
        variants={fadeIn("", "", 0.1, 1)}
        className={`${styles.sectionText}  `}
      >
        Hi, my name is Farell. I'm currently a student at the University of
        Texas at Dallas. I'm majoring in business analytics and minoring in
        software engineer. The reason I created this portfolio is to showcase my
        passion for programming and my ability to design. Below are the skills
        that I possess as a software engineer.
      </motion.p>

      {device.isMobile ? (
        <About_Corousel />
      ) : (
        <div>
          <motion.div className="mt-20 flex flex-wrap gap-10 justify-center items-center">
            {services.map((service, index) => {
              return (
                <ServiceCard key={service.title} index={index} {...service} />
              );
            })}
          </motion.div>
        </div>
      )}
    </div>
  );
};

export default sectionWrapper(About, "about");
