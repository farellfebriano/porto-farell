import {
  carcar,
  sherlockLocksmith,
  wallmart,
  cunninghamJournal,
  Frontend,
  Datascience,
  Backend,
  Bussines,
  docker,
  git,
  javascript,
  reactjs,
  redux,
  threejs,
  pyton,
  ai,
  TalkNow,
  github,
  gitlab,
  linkedin,
  utd,
  tcc,
  ccc,
  mahatma,
  hackReactor,
  java,
  C,
  MySQL,
  FastApi,
  rabbitMQ,
  django,
  nextJs,
  webSocket,
  scrumptious,
  alpha,
  netflix,
  matrix,
  movieRaiders,
} from "../assets";
export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "projects",
    title: "Projects",
  },
  {
    id: "experience",
    title: "Experience",
  },
  {
    id: "contact",
    title: "Contact",
  },
];
//about
export const services = [
  {
    title: "Frontend",
    icon: Frontend,
  },
  {
    title: "Backend",
    icon: Backend,
  },
  {
    title: "Data Science",
    icon: Datascience,
  },
  {
    title: "Business Analysis",
    icon: Bussines,
  },
];
// experience
export const experienceStory = {
  Education:
    "My journey into the world of coding started during my first Java class while pursuing a degree in Business Analysis. Although changing my major wasn't a feasible option at that time, I was determined to explore this newfound passion. So, I started learning by myself, I enrolled in a coding boot camp, and I decided to minor in Software Engineering. Those decisions made me a unique and well-rounded engineer who spans both the technical and business facets of the field. The following highlights my educational journey:",
  Work: "I love taking on challenges and have a strong work ethic that drives me to excel in any task that I do. I thrive in environments that require problem-solving, adaptability, and a commitment to achieving goals.  I believe that facing challenges  not only leads to personal growth but also encourages innovation and success in any part of life.",
};

export const experiences = {
  Work: [
    {
      title: "Locksmith Technician",
      company_name: "Sherlock Locksmith",
      icon: sherlockLocksmith,
      date: "Mar 2022 - Present",
      points: [
        "Proficiently programming transponder chips into Electronic Control Units (ECUs) to generate new keys, showcasing expertise in automotive security systems and key management.",
        "Experimented the adoption of technology innovations, including the implementation of Universal key generation, resulting in substantial time and cost savings by eliminating the need to purchase a variety of keys for each model of car.",
      ],
    },
    {
      title: "Waiter & Chef",
      company_name: "Cunninghams Journal",
      icon: cunninghamJournal,
      iconBg: "#E6DEDD",
      date: "Jan 2021 - Feb 2022",
      points: [
        "Improved table management, reducing waiting time by 45%, while ensuring consistent food quality resulted in satisfied costumer and zero negative complaints.",
        "Managed and trained 5 new employees about customer service policies, conflict resolution, payment processing, and opening/closing procedures",
      ],
    },
    {
      title: "Cashier",
      company_name: "Wallmart",
      icon: wallmart,
      iconBg: "#383E56",
      date: "2019 - March 2021",
      points: [
        "Effectively processed customer purchases, ensuring accurate scanning and itemization of products.",
        "Maintained a high level of accuracy in cash handling, minimizing errors and discrepancies in cash register transactions.",
        "Collaborated with team members to maintain a clean and organized checkout area, ensuring a pleasant shopping experience for customers.",
      ],
    },
  ],
  Education: [
    {
      title: "University of Texas at Dallas",
      company_name: "UTD",
      icon: utd,
      iconBg: "#383E56",
      date: "Jan 2022 - Dec 2025 Expected",
      points: [
        "Majoring in Business Analytics made me learn skills such as data analysis, statistical analysis, data visualization, predictive modeling, data management, and the ability to make data-driven decisions to support business objectives. This gives me the skills to understand how businesses use data as a basis for their decision-making.",
        "Minoring in Software Engineering even made me more in love with programming and technology. It gave me the basic knowledge to learn more in the future. ",
      ],
    },
    {
      title: "HackReactor",
      company_name: "Software Enginier BootCamp",
      icon: hackReactor,
      iconBg: "#383E56",
      date: "May 2022 - Sep 2022",
      points: [
        "A well-known coding boot camp that offers immersive software engineering programs designed to prepare individuals for careers in the tech industry.  ",
        "The programs make me engage in hands-on coding projects and collaborate on real-world software development tasks. ",
      ],
    },
    {
      title: "Tarant Comunity College",
      company_name: "TCC",
      icon: tcc,
      iconBg: "#E6DEDD",
      date: "Jan 2021 - Feb 2021",
      points: [
        "The first community college that I attended when I first moved to Texas. I took two semesters of prerequisite classes at this College.",
      ],
    },
    {
      title: "Nebraska Central Comunity Colege ",
      company_name: "CCC",
      icon: ccc,
      iconBg: "#383E56",
      date: "Aug 2019 - Dec 2020",
      points: [
        "This college was the first college that I ever attended when I was living in Nebraska. I spent three semesters in this college.",
      ],
    },
    {
      title: "Mahatma Gading School",
      company_name: "MGS",
      icon: mahatma,
      iconBg: "#E6DEDD",
      date: "2013 - 2019",
      points: [
        "One of the three best schools in North Jakarta, Indonesia.",
        "Spend seven to twelve grades in this school. I learned the meaning of friendship, the beauty of diversity, and hard work in this school ",
      ],
    },
  ],
};
export const technologies = [
  {
    name: "Python",
    icon: pyton,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "Java",
    icon: java,
  },
  {
    name: "C",
    icon: C,
  },
  {
    name: "MySQL",
    icon: MySQL,
  },
  {
    name: "Fast Api",
    icon: FastApi,
  },

  {
    name: "Django",
    icon: django,
  },

  {
    name: "NextJs",
    icon: nextJs,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "RabbitMQ",
    icon: rabbitMQ,
  },
  {
    name: "Redux",
    icon: redux,
  },

  {
    name: "Web Socket",
    icon: webSocket,
  },
  {
    name: "Three JS",
    icon: threejs,
  },
  {
    name: "Git",
    icon: git,
  },

  {
    name: "Docker",
    icon: docker,
  },
];

export const projects = {
  //pass project
  Past: [
    {
      name: "Scrumptious",
      description:
        "My first time to get exposed to frameworks and also my first project. In this project, we created a food menu website where users can browse a menu that is created by another user. I also implemented an authentication system with Django build authenticator. ",
      tags: [
        {
          name: "Django",
          color: "matrix",
        },
        {
          name: "HTML",
          color: "matrix",
        },
        {
          name: "CSS",
          color: "matrix",
        },
      ],
      image: scrumptious,
      source_code_link: "https://gitlab.com/supreme6122018/scrumptious_project",
      rep: gitlab,
    },
    {
      name: "Project Alpha",
      description:
        "This project is intended to make me to be more familiar with Django framework. In this project, I created a task manager website where users can schedule and create a deadline for every task.",
      tags: [
        {
          name: "Django",
          color: "matrix",
        },
        {
          name: "HTML",
          color: "matrix",
        },
        {
          name: "CSS",
          color: "matrix",
        },
      ],
      image: alpha,
      source_code_link: "https://gitlab.com/farellfebriano/project-alpha",
      rep: gitlab,
    },
    {
      name: "CarCar",
      description:
        "In this project, I implemented microservice techniques as a way to do horizontal scaling. This website creates a service and sales manager system where Car dealer owner can manage their car, service, and data of their salespeople. ",
      tags: [
        {
          name: "MicroService",
          color: "matrix",
        },
        {
          name: "Django",
          color: "matrix",
        },
        {
          name: "React",
          color: "matrix",
        },
        {
          name: "Docker",
          color: "matrix",
        },
      ],
      image: carcar,
      source_code_link: "https://gitlab.com/farellfebriano/car-car",
      rep: gitlab,
    },
    {
      name: "Netflix ",
      description:
        "In this project, I want to focus on learning Netflix design as my reference for my next project, Movie Raiders. I also used movie API for the data resource.",
      tags: [
        {
          name: "React",
          color: "matrix",
        },
        {
          name: "HTML",
          color: "matrix",
        },
        {
          name: "tailwind",
          color: "matrix",
        },
      ],
      image: netflix,
      source_code_link: "https://gitlab.com/farellfebriano/Netflix",
      rep: gitlab,
    },
  ],
  Recent: [
    {
      name: "Movie Raiderss",
      description:
        "In this project, I led a team of three people in creating a movie rating website, implementing PostgreSQL as the database and JWT for authentication. The team achieved a 95% score and was nominated as one of the top 3 frontend projects of the semester.",
      tags: [
        {
          name: "FastApi",
          color: "matrix",
        },
        {
          name: "React",
          color: "matrix",
        },
        {
          name: "Redux",
          color: "matrix",
        },
        {
          name: "WebSocket",
          color: "matrix",
        },
      ],
      image: movieRaiders,
      source_code_link:
        "https://gitlab.com/farellfebriano/module-3-project-gamma-2",
      rep: gitlab,
    },
    {
      name: "Matrix Rain",
      description:
        "Who doesn't love 'The Matrix,' the movie? This project forms the central concept of this portfolio. It had a key issue: the pattern would stop working if the user resized the window. Fortunately, this issue has been resolved and integrated into this portfolio.",
      tags: [
        {
          name: "CSS",
          color: "matrix",
        },
        {
          name: "HTML",
          color: "matrix",
        },
        {
          name: "JavaScript",
          color: "matrix",
        },
      ],
      image: matrix,
      source_code_link: "https://gitlab.com/farellfebriano/matrix-rain",
      rep: gitlab,
    },
  ],
  Future: [
    {
      name: "TalkNow",
      description:
        "In this project, I have created a chat application where people can send messages, voice messages, make video calls, share pictures, and more.",
      tags: [
        {
          name: "OnProgres",
          color: "matrix",
        },
      ],
      image: TalkNow,
      source_code_link: "",
      rep: gitlab,
    },
    {
      name: "Time Machine",
      description:
        "In this project, I will create a chat system where people can engage in conversations with historical figures from the past, such as Albert Einstein, George Washington, and Abraham Lincoln. I will utilize AI to mimic the chosen character's persona",
      tags: [
        {
          name: "FutureProject",
          color: "matrix",
        },
      ],
      image: ai,
      source_code_link: "",
      rep: gitlab,
    },
  ],
};

//footer
export const footerAccount = [
  { logo: linkedin, link: "https://www.linkedin.com/in/farell-febriano/" },
  { logo: gitlab, link: "https://gitlab.com/farellfebriano" },
  { logo: github, link: "https://github.com/farellfebriano" },
];
export const footerContact = [{}];
export const footerLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "experience",
    title: "Experience",
  },
  {
    id: "tech",
    title: "Tech",
  },
  {
    id: "projects",
    title: "Projects",
  },
  {
    id: "contact",
    title: "Contact.",
  },
];
