# What the idea behind this creation:

- The idea of this portfolio is the movie matrix. Since I am a software engineer, the concept matrix is suited since the movie's storyline is all about computers and code. One of the main concepts is using the matrix raining code and using glass morphism for the text and also the card.

# The challenge of creating this porofolio

- This portfolio took 1 month to develop and there were a lot of challenges since there are a lot of new concepts that are new to me. However, there are 1 main challenges in creating this matrix portfolio.

- 1. The first challenge is the background. The background itself took 3 days to make since it has a problem with resizing. There are a lot of resources that are provided on the internet for creating the matrix background. However, every single one of them has something in common, It will stop or error when the users resize the page. After researching methodologies I found an article that helps me to fix the problem. resource:(https://dev.to/elijahtrillionz/cleaning-up-async-functions-in-reacts-useeffect-hook-unsubscribing-3dkk)

# Libraxy that i used for creating this portofolio

--legacy-peer-deps (library that require older react version)
@react-three/fiber (three.js in react )
@react-three/drei
maath
react-tilt (tilting library)
react-vertical-timeline-component (for the time line) @emailjs/browser (sending email trough js)
framer-motion
react-router-dom (routers)
react-simple-typewriter

# Redeploy documentation

If you want to change the web you need to go to the potainer and change the container called porto

```bash
http://farellfebriano.com:9000/#!/2/docker/dashboard
```

1. and also dont forget to create dist

```bash
npm run build
```

2. Create the docker image

```bash
docker build -t farellfebriano/port1 --no-cache .
```

3. push the image to the cloud

```bash
docker push farellfebriano/port1
```
